
import 'package:flutter/material.dart';

import "G.dart" as G;
import "style.dart" as S;

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    double radius = 8;
    return Drawer(
      child: ListView(

        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Column (
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(G.PROJECT_NAME,
                  style: TextStyle(
                    fontFamily: S.LOGO_FONT_NAME,
                    fontSize: 42.0,
                    color: Colors.white
                  )
                ),
                Container(width: 1, height: 10,),
                Text('${G.WWW_SITE}', style: TextStyle(color: Colors.white),),
                Text('v${G.VERSION}', style: TextStyle(color: Colors.grey),),
              ],
            ),
            decoration: BoxDecoration(
              color: S.main_color,
            ),
          ),
          ListTile(
            title: Text('Home'),
            leading: CircleAvatar(backgroundColor: S.main_color, maxRadius: radius,),
            onTap: () {
              Navigator.popAndPushNamed(context, '/');
            },
          ),

          ListTile(
            title: Text('Map'),
            leading: CircleAvatar(backgroundColor: Colors.green, maxRadius: radius,),
            onTap: () {
              Navigator.popAndPushNamed(context, '/map');
            },
          ),



        ],
      ),
    );

  }
}
