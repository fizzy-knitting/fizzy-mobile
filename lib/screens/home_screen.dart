
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';

import "../G.dart" as G;
import "../style.dart" as S;
import '../drawer.dart' as drawer;

//import "../jobs/job_screen.dart";




const String _markdownData = """# Welcome to Fizzy Knitting

**The UK Railway Electrification map project**

This project is changing and maturing, so for more info
visit the website.

[fizzy-knitting.gitlab.io](https://fizzy-knitting.gitlab.io)

""";

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);


  @override
  HomeScreenState createState() => new HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {


  @override
  void initState(){

  }



  @override
  Widget build(BuildContext context) {


    return new Scaffold(
      appBar: new AppBar(
        title: new Text(G.PROJECT_NAME),
      ),

      drawer: drawer.AppDrawer(),

      body: Padding(
        padding: EdgeInsets.fromLTRB(2, 10, 0, 2),
        child:  Column(
          children: <Widget>[

            Expanded(
              child:const Markdown(data: _markdownData)
            )
          ],
        )
      )


    );
  }
}
