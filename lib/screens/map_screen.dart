
import 'package:flutter/material.dart';
import 'package:latlong/latlong.dart';
import 'package:flutter_map/flutter_map.dart';

import "../G.dart" as G;
import "../style.dart" as S;
import '../drawer.dart' as drawer;

var mapTopLeftLimit = LatLng(58.0, -7.0);
var mapBottomRightLimit = LatLng(50.0, 2.22);
var mapCenter = LatLng( 54.0728, -1.538);

class MapScreen extends StatefulWidget {
  MapScreen({Key key}) : super(key: key);


  @override
  MapScreenState createState() => new MapScreenState();
}

class MapScreenState extends State<MapScreen> {


  @override
  void initState(){

  }



  @override
  Widget build(BuildContext context) {


    return Scaffold(
      appBar: AppBar(
        title: Text("Fizzy Map"),
      ),

      drawer: drawer.AppDrawer(),

      body:  FlutterMap(
        options: MapOptions(
          debug: true,
          center: mapCenter,
          zoom: 6.0,
          //nePanBoundary: mapTopLeftLimit,
          //swPanBoundary: mapBottomRightLimit
          crs: const Epsg3857()
        ),
        layers: [
          new TileLayerOptions(
            urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            subdomains: ['a', 'b', 'c'],
            opacity: 0.5
          ),
          new TileLayerOptions(
            urlTemplate: "https://www.railwayhistory.org/electric/tiles/vector/{z}/{x}/{y}",
            //subdomains: ['a', 'b', 'c'],
            //opacity: 0.3

          ),
          new MarkerLayerOptions(
            markers: [
//              new Marker(
//                width: 80.0,
//                height: 80.0,
//                point: new LatLng(51.5, -0.09),
//                builder: (ctx) =>
//                new Container(
//                  child: new FlutterLogo(),
//                ),
//              ),
            ],
          ),
        ],
      )


    );
  }
}
